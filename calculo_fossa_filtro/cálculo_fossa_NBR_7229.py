from os import close


def tabela_1():
    tabela_1_permanente=[[160,1],[130,1],[100,1],[100,1],[80,1]]
    tabela_1_temporario=[[70,0.30],[50,0.20],[6,0.10],[25,0,10],[2,0.02],[480,4.0]]

    ind_tab_1 = int(input("Digite:\n 1 para Ocupantes permanentes \n 2 para Ocupantes temporários\n: "))
    while ind_tab_1 < 1 or ind_tab_1 > 2:
        print("número inválido")
        ind_tab_1 = int(input("Digite:\n 1 para Ocupantes permanentes \n 2 para Ocupantes temporários\n: "))
    if ind_tab_1 == 1:
        tipo_tab_1 = int(input("Digite:\n 1 para residências \n 2 para hoteis \n 3 para alojameno provisório\n: "))
        while tipo_tab_1 < 1 or tipo_tab_1 > 3:
            print("número inválido")
            tipo_tab_1 = int(input("Digite:\n 1 para residências \n 2 para hoteis \n 3 para alojameno provisório\n: "))
        if tipo_tab_1 == 1:
            tipo_res = int(input("Digite:\n 1 para residências de padrão alto \n 2 para residências de padrão médio \n 3 para residências de padrão baixo\n: "))
            while tipo_res < 1 or tipo_res > 3:
                print("número inválido")
                tipo_res = int(input("Digite:\n 1 para residências de padrão alto \n 2 para residências de padrão médio \n 3 para residências de padrão baixo\n: "))
            if tipo_res == 1:
                c = tabela_1_permanente[0][0]
                lf = tabela_1_permanente[0][1]
            else:
                if tipo_res == 2:
                    c = tabela_1_permanente[1][0]
                    lf = tabela_1_permanente[1][1]
                else:
                    c = tabela_1_permanente[2][0]
                    lf = tabela_1_permanente[2][1]
        else:            
            if tipo_tab_1 == 2:
                    c = tabela_1_permanente[3][0]
                    lf = tabela_1_permanente[3][1]
            else:
                    c = tabela_1_permanente[4][0]
                    lf = tabela_1_permanente[4][1]
                
    else:
        if ind_tab_1 == 2:
            tipo_tab_2 = int(input("Digite:\n 1 para fábrica em geral \n 2 para escritório \n 3 para edifícios públicos \n 4 para escolas \n 5 para bares \n 6 para rastaurantes e similares \n 7 para cinemas e teatros \n 8 pra sanitários públicos\n: "))
            while tipo_tab_2 < 1 or tipo_tab_2 > 8:
                print("número inválido")
                tipo_tab_2 = int(input("Digite:\n 1 para fábrica em geral \n 2 para escritório \n 3 para edifícios públicos \n 4 para escolas \n 5 para bares \n 6 para rastaurantes e similares \n 7 para cinemas e teatros \n 8 pra sanitários públicos\n: "))
            if tipo_tab_2 == 1:
                c = tabela_1_temporario[0][0]
                lf = tabela_1_temporario[0][1]
            else:
                if tipo_tab_2 == 2 or tipo_tab_2 == 3 or tipo_tab_2 == 4:
                    c = tabela_1_temporario[1][0]
                    lf = tabela_1_temporario[1][1]
                else:
                    if tipo_tab_2 == 5:
                        c = tabela_1_temporario[2][0]
                        lf =tabela_1_temporario[2][1]
                    else:
                        if tipo_tab_2 == 6:
                            c = tabela_1_temporario[3][0]
                            lf = tabela_1_temporario[3][1]
                        else:
                            if tipo_tab_2 == 7:
                                c = tabela_1_temporario[4][0]
                                lf = tabela_1_temporario[4][1]
                            else:
                                if tipo_tab_2 ==8:
                                    c = tabela_1_temporario[5][0]
                                    lf = tabela_1_temporario[5][1]
    return c, lf
tabela_2=[[1.00,24],[0.92,22],[0.83,22],[0.75,18],[0.67,16],[0,58,14],[0.50,12]]
tabela_3=[[94,65,57],[134,105,97],[174,145,137],[214,185,177],[254,225,217]]

def main():
    n_usuarios = int(input("Digite o número de contribuintes: "))
    c, lf = tabela_1()
    int_limp = int(input("Informe o intervalo de limpeza em anos: "))
    temp_amb = float(input("Informe a temperatura ambiente em ºC: "))
                 
    if c*n_usuarios < 1500:
        t = tabela_2[0][0]
    else:
            if c*n_usuarios > 1500 and c*n_usuarios < 3001:
                t = tabela_2[1][0]
            else:
                if c*n_usuarios >= 3001 and c*n_usuarios < 4501:
                    t = tabela_2[2][0]
                else:
                    if c*n_usuarios >= 4501 and c*n_usuarios < 6001:
                        t = tabela_2[3][0]
                    else:
                        if c*n_usuarios >= 6001 and c*n_usuarios < 7501:
                            t = tabela_2[4][0]
                        else:
                            if c*n_usuarios >= 7501 and c*n_usuarios < 9001:
                                t = tabela_2[5][0]
                            else:
                                t = tabela_2[6][0]
    if int_limp == 1 and temp_amb <= 10.0:
        k = tabela_3[0][0]
    else:
            if int_limp == 1 and (temp_amb >10 and temp_amb <=20):
                k = tabela_3[0][1]
            else:
                if int_limp == 1 and temp_amb > 20:
                    k = tabela_3[0][2]
                else:
                    if int_limp == 2 and temp_amb <= 10.0:
                        k = tabela_3[1][0]
                    else:
                        if int_limp == 2 and (temp_amb >10 and temp_amb <=20):
                            k = tabela_3[1][1]
                        else:
                            if int_limp == 2 and temp_amb > 20:
                                k = tabela_3[1][2]
                            else:
                                if int_limp == 3 and temp_amb <= 10.0:
                                    k = tabela_3[2][0]
                                else:
                                    if int_limp == 3 and (temp_amb >10 and temp_amb <=20):
                                        k = tabela_3[2][1]
                                    else:
                                        if int_limp == 3 and temp_amb > 20:
                                            k = tabela_3[2][2]
                                        else:
                                            if int_limp == 4 and temp_amb <= 10.0:
                                                k = tabela_3[3][0]
                                            else:
                                                if int_limp == 4 and (temp_amb >10 and temp_amb <=20):
                                                    k = tabela_3[3][1]
                                                else:
                                                    if int_limp == 4 and temp_amb > 20:
                                                        k = tabela_3[3][2]
                                                    else:
                                                        if int_limp == 5 and temp_amb <= 10.0:
                                                            k = tabela_3[4][0]
                                                        else:
                                                            if int_limp == 5 and (temp_amb >10 and temp_amb <=20):
                                                                k = tabela_3[4][1]
                                                            else:
                                                                if int_limp == 5 and temp_amb > 20:
                                                                    k = tabela_3[4][2]
    vol = 1000 + n_usuarios * ((c*t) + (k*lf))

    if vol/1000 <= 6.0:
        prof_min = 1.20
        prof_max = 2.20
    else:
        if vol/ 1000 > 6.0 and vol/ 1000 <= 10.0:
            prof_min = 1.50
            prof_max = 2.50
        else:
            prof_min = 1.80
            prof_max = 2.80


    print(" O Volume total do tanque é ", vol/1000, "m³ \n A profundidade mínima é de ", prof_min, " Metros \n A profundidade máxima é de ", prof_max, "metros")
    
print("Este programa realiza cáculo de dimensionamento de sistemas de tanques sépticos de acordo com a NBR 7229")
saida = int(input("Pressione qualquer número pra continuar ou 0 para voltar: "))
while saida != 0:
    main()
    saida = int(input("Pressione qualquer número pra continuar ou 0 para voltar: "))
    if saida == 0:
        exit()